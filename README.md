# Career landing page #

Wszystkie komendy należy uruchomić z poziomu katalogu głównego całego projektu, chyba że napisano inaczej.

# TL;DR #

## Wymagania poczatkowe ##
Zainstaluj npm: https://nodejs.org/en/

Następnie z linii poleceń kolejno:
```
npm install gulp-cli -g
npm install gulp -D
```

## Zbudowanie projektu ##
```
npm install
gulp build
```
Pliki wynikowe znajdą się w **./build**

# Wersja dłuższa #

## Wymagania ##

### npm ##

url: https://nodejs.org/en/ 

npm jest częścią pakietu Node.js - należy pobrać pakiet korzystając z powyższego adresu, a po instalacji sprawdzić poprawność instalacji wpisując w linii komend:

```
node -v
npm -v
```
w obu przypadkach prawidłową odpowiedzią jest numer zainstalowanej wersji

### gulp cli ##

url: https://gulpjs.com/

do budowania projektu wykorzystane zostały Gulp oraz Gulp CLI, które można teraz zainstalować:

```
npm install gulp-cli -g
npm install gulp -D
```

### instalacja wymaganych pakietów ###

```
npm install
```

## Automatyczne budowanie projektu ##

należy uruchomić z linii poleceń:

```
gulp
```
Spowoduje to zbudowanie projektu (pliki wynikowe dostępne w katalogu **./build**), oraz automatyczne śledzenie zmian w plikach *.njk, *.html, *.js, *.sass. Zostanie otwarta strona (domyślny adres http://localhost:3000) w domyślnej przeglądarce.  

##### Opcjonalnie #####
można też wykonać jednokrotne budowanie projektu (bez śledzenia zmian w plikach) za pomocą komendy:

```
gulp build
```

## Modyfikowanie szablonów ##

Do systemu szablonów został wykorzystany nunjucks https://mozilla.github.io/nunjucks/templating.html

Szablony znajdują się w katalogu **./src/templates**
