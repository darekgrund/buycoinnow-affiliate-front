'use strict';
const defaultConfig = {
    path: {
        src: {
            scss: [
                './resources/assets/styles/**/*.scss'
            ]
        },
        build: {
            prod: {
                dst: './prod'
            },
            dev: {
                dst: './dev'
            }
        }
    },
    languages: [
        'en'
    ],

    env: '',
    assetsPath: '.',
    assetsVersion: Date.now(),
    language: ''
};

let config = defaultConfig;

let gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    clip = require('gulp-clip-empty-files'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    nunjucksRender = require('gulp-nunjucks-render'),
    sassVariables = require('gulp-sass-variables'),
    sourcemaps = require('gulp-sourcemaps'),
    debug = require('fancy-log'),
    mergeJson = require('gulp-merge-json'),
    data = require('gulp-data'),
    sequence = require('run-sequence'),
    del = require('del'),
    gulpif = require('gulp-if'),
    fs = require('fs'),
    path = require('path');


/**
 * Everything that ends in ./build directory is done here
 */

// Scripts
gulp.task('scripts', function() {
    return gulp.src('./resources/assets/scripts/*.js')
        .pipe(plumber())
        .pipe(gulpif(config.env === 'prod', uglify()))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(config.path.build.dst + '/scripts'))
        .pipe(browserSync.stream({once: true}));
});


// Styles
gulp.task('styles', function() {
    return gulp.src(config.path.src.scss)
        .pipe(clip())
        .pipe(sassVariables({
            $assetsVersion: config.assetsVersion
        }))
        .pipe(gulpif(config.env === 'dev', sourcemaps.init()))
        .pipe(sass({
            outputStyle: config.env === 'dev' ? 'expanded' : 'compressed'
        }).on('error', sass.logError))
        .pipe(gulpif(config.env === 'dev', sourcemaps.write()))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(config.path.build.dst + '/css/'))
        .pipe(reload({stream:true}));
});


// Copy files
gulp.task('copy', function() {
    return gulp.src([
        './resources/assets/vendor/**/*.*',
        './resources/assets/images/**/*.*',
        './resources/assets/fonts/**/*.*',
        './resources/assets/files/**/*.*',
        './resources/assets/config/**/*.*'
    ], {base: 'resources/assets'
    }).pipe(gulp.dest(config.path.build.dst));
});


// Delete build
gulp.task('clean:build', function () {
    return del(config.path.build.dst);
});


// Browser sync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: config.path.build.dst,
            directory: true
        }
    });
});


// Compile nunjucks files
gulp.task('nunjucks', function() {
    return gulp.src('./resources/templates/pages/**/*.+(html|njk)')
        .pipe(plumber())
        .pipe(data(function () {
            return {
                config: config
            }
        }))
        .pipe(nunjucksRender({
            path: ['./resources/templates']
        }))
        .pipe(gulp.dest(config.path.build.dst))
        .pipe(browserSync.stream({once: true}));
});


// Build project
gulp.task('build', function () {
    sequence(
        ['clean:build'],
        ['copy', 'scripts', 'styles'],
        'nunjucks'
    );
});


// Watch file changes
gulp.task('watch', function () {
    gulp.watch("./resources/assets/scripts/*.js", ['scripts']);
    gulp.watch(config.path.src.scss, ['styles']);
    gulp.watch("./resources/templates/**/*.+(html|njk)", ['nunjucks']);
});

let setConfigEnv = function (env) {
    config.env = env;
    config.language = defaultConfig.languages[0];

    switch (env) {
        case 'dev':
            config.path.build.dst = defaultConfig.path.build.dev.dst;
            break;

        case 'prod':
            config.path.build.dst = defaultConfig.path.build.prod.dst;
            break;
    }
};


gulp.task('switch:dev', function () {
    setConfigEnv('dev');
});


gulp.task('switch:prod', function () {
    setConfigEnv('prod');
});


// Default task
gulp.task('default', function () {
    sequence('dev', 'browser-sync', 'watch');
});


gulp.task('dev', function () {
   sequence('switch:dev', 'build');
});


gulp.task('prod', function () {
    sequence('switch:prod', 'build');
});
