'use strict';
(function ($) {
    // Tabs
    var $tabsContainer = $('.js-tabs');
    $tabsContainer.on('click', '.js-tabs-link', function (e) {
        var $tabs = $(this).closest('.js-tabs');
        $tabs.find('.tabs__item--active').removeClass('tabs__item--active');
        $(this).closest('.js-tabs-item').addClass('tabs__item--active');
        e.preventDefault();
    });

    // FAQ
    var $faq = $('.js-faq-container');
    var $faqSlick = $faq.closest('.js-slick-container').find('.js-slick-slides:eq(0)');
    $faq.on('click', '.js-faq-toggle', function () {
        var $card = $(this).closest('.js-faq-card');
        if ($card.hasClass('faq__card--opened')) {
            $card.removeClass('faq__card--opened');
        } else {
            $(this).closest('.js-faq').find('.faq__card--opened').removeClass('faq__card--opened');
            $card.addClass('faq__card--opened');
        }

        setTimeout(function () {
            $faqSlick.trigger('afterChange');
        }, 300);
    });
})(jQuery);
